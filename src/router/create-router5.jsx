//create-router5.js
import {createRouter} from 'router5';
import loggerPlugin from 'router5/plugins/logger';
import browserPlugin from 'router5/plugins/browser';
import {mobxPlugin} from 'mobx-router5';

import routerStore from 'stores/routerStore';
import React from "react";
import ErrorPage from "Pages/ErrorPage";
import * as Rmr from "react-mobx-router5";


/**
 * Замена стандартного метода, чтобы обрабатывать синхронные и асинхронные ошибки
 */
const oldRender = Rmr.RouteView.prototype.render;


Rmr.RouteView.prototype.render = function () {
    // Синхронный перехватчик ошибок

    if (this.state.hasError) {

        return <ErrorPage {...this.props}/>;
    }

    // асинхронный компонент вызывается через
    // декоратор asyncErrorDecorator
    // и оборачивает отдельно каждую страницу
    return oldRender.apply(this)
};


export default function configureRouter(defaultRoute, routesMap, useLoggerPlugin = false) {
    const router = createRouter(routesMap, {defaultRoute: defaultRoute, allowNotFound: false});
    router.usePlugin(mobxPlugin(routerStore)); // Important: pass the store to the plugin!
    router.usePlugin(browserPlugin({useHash: false}));
    routerStore.defaultRoute = defaultRoute;
    if (useLoggerPlugin) {
        router.usePlugin(loggerPlugin);
    }
    return router;
}
