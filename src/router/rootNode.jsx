import React from "react";
import PropTypes from 'prop-types';
import {routeNode, RouteView} from "react-mobx-router5";
import routesMap from "Routes";

/*
Вместо того, чтобы создавать nodecomponents:
    children: [
        {
            name: "attributeGroup",
            path: "/attribute-group",
            component: rootNode("channels.attributeGroup"), <---
            children: [
                {
 */
const rootNode = (routeNodeName) => {
    class RootNode extends React.Component {
        render() {
            const {route} = this.props;
            return (
                <RouteView
                    route={route}
                    routes={routesMap}
                    routeNodeName={routeNodeName}
                />
            )
        }
    }

    // All injected by routeNode
    RootNode.propTypes = {
        route: PropTypes.object, // observable
        plainRoute: PropTypes.object, // non-observable plain js obj
        routerStore: PropTypes.object
    };

    return routeNode(routeNodeName)(RootNode);
};


export default rootNode
