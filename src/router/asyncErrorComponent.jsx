import {action, observable} from "mobx";
import {observer, Provider} from "mobx-react";
import React from "react";
import ErrorPage from "Pages/ErrorPage";

class AsyncErrorStore {
    @observable hasError = false;
    code = undefined;

    constructor({code, hasError}) {
        // code and hasError from updated state in permissions middleware and etc.
        if (hasError) {
            this.setError(code)
        }

    }

    @action setError(code) {
        this.code = code;
        this.hasError = true
    }

    @action unSetError() {
        this.code = "";
        this.hasError = false;
    }
}


class AsyncErrorComponent extends React.Component {
    constructor(props) {
        super(props);
        this.store = new AsyncErrorStore(this.props.route || {})
    }

    render() {
        const {children, route} = this.props;

        if (route && this.store.hasError) {

            return <ErrorPage errorCode={this.store.code}/>
        }

        return (
            <Provider asyncErrorStore={this.store}>
                {children}
            </Provider>
        )
    }
}

/**
 * При помощи этого декоратора происходит ловля асинхронных ошибок из промисов
 *
 * !Важен порядок декораторов
 * @asyncErrorDecorator() - сначала идет декоратор
 * @inject("some other")
 * @inject("some other store")
 * @inject("asyncErrorStore") - затем asyncErrorStore для callback-в, который он использует на странице:
 *
 * this.asyncErrorStore.setError(code)
 */
function asyncErrorDecorator() {

    return function (Child) {

        return class extends React.Component {

            render() {
                return <AsyncErrorComponent {...this.props}>
                    <Child {...this.props}/>
                </AsyncErrorComponent>
            }
        }
    }

}

export default asyncErrorDecorator