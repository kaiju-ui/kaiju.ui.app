import React from "react";
import {inject, observer} from "mobx-react";
import "./LoginForm.scss"


@inject("authStore")
@inject("userStore")
@inject("routerStore")
@observer
export default class LoginForm extends React.Component {
    loadButtonClass = 'm-loader m-loader--light m-loader--right';

    handleUsernameChange = e => {
        this.props.authStore.setUsername(e.target.value);
    };

    handlePasswordChange = e => {
        this.props.authStore.setPassword(e.target.value);
    };

    redirectToReferer() {
        let referer = this.props.routerStore.route.params.referer;

        if (!referer) {
            this.props.routerStore.router.navigateToDefault();
            return
        }

        this.props.routerStore.router.navigate(
            referer.name, {...referer.params},
            ((error) => {
                    if (error) {
                        console.error("RedirectError: ", error);
                        this.props.routerStore.router.navigateToDefault();
                    }
                }
            )
        );
    }

    handleSubmitForm = e => {
        e.preventDefault();
        this.props.authStore.login()
            .then((userData) => {
                if (!userData) {
                    return
                }

                this.props.userStore.setUser(userData);
                this.props.userStore.fetchSettings().then(
                    () => {
                        this.redirectToReferer();
                        this.props.authStore.reset();
                    }
                )
            });
    };

    render() {
        const {values, error, isFetching} = this.props.authStore;

        return (
            <div className="m-login__signin">
                <form className="m-login__form m-form" onSubmit={this.handleSubmitForm}>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input"
                               pattern="[\w_]+"
                               type="text" placeholder={utils.getTranslation("Login.username")}
                               value={values.username}
                               onChange={this.handleUsernameChange}
                               autoComplete="true"
                        />
                    </div>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input m-login__form-input--last"
                               autoComplete="true"
                               value={values.password}
                               onChange={this.handlePasswordChange}
                               type="password"
                               placeholder={utils.getTranslation("Login.password")}/>
                    </div>
                    <div style={{minHeight: "40px", display: "block"}}>
                        <p className="text-danger"
                           style={{textAlign: "center", marginTop: "20px"}}>{error}</p>
                    </div>
                    <div className="m-login__form-action" style={{marginTop: "0"}}>
                        <button id="m_login_signin_submit"
                                onClick={this.handleSubmitForm}
                                disabled={isFetching}
                                style={{textTransform: "uppercase", cursor: "pointer"}}
                                className={`btn btn-success m-login__btn m-login__btn--success ${isFetching === true ? this.loadButtonClass : ""}`}>
                            {utils.getTranslation("Login.login")}
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}