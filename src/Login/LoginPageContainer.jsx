import React from "react";
import {inject} from "mobx-react";
import {routeNode} from "react-mobx-router5";
import "./LoginPageContainer.scss"
import LoginForm from "./LoginForm";

const routeNodeName = 'login';


@inject("routerStore")
class LoginPageContainer extends React.Component {

    componentWillUnmount() {
        document.getElementById("body").style.backgroundColor = "#fff";
    }

    render() {
        document.getElementById("body").style.backgroundColor = "#21252C";
        let motto = this.props.routerStore.projectMotto();
        let projectLabel = this.props.routerStore.projectLabel();

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page login-container">
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2"
                    id="m_login">
                    <div className="m-grid__item m-grid__item--fluid m-login__wrapper">
                        <div className="m-login__container">
                            <div className="m-login__logo" style={{fontSize: "40px"}}>
                                [{projectLabel && projectLabel}]
                                <div className="m-login__head mt-4">
                                    {motto && <h3 className="m-login__title">{motto}</h3>}
                                </div>
                            </div>
                            <LoginForm/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default routeNode(routeNodeName)(LoginPageContainer);
