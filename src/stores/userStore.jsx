import {action, computed, observable, toJS} from 'mobx';
import Axios from "axios";


class UserStore {
    @observable user;

    constructor() {
        this.user = null;
        this.refresh = false;
        this.settingsCallback = undefined;
        this.hash = undefined;
        this.refreshInterval = 5 * 60 * 1000;
        this.timeout = undefined;
    }

    @action
    getCurrentUser() {
        return Axios.post('/public/rpc', {
            method: "auth.user_info"
        }).then(
            (response) => {
                if (response.data.result) {
                    this.setUser(response.data.result)
                } else {
                    this.setUser(null)
                }
                return this.user
            }
        ).catch((e) => {
                console.error("LOGIN ERROR: ", e);
                this.setUser(null)
            }
        )
    }

    @computed get isLoggedIn() {
        return Boolean(toJS(this.user))
    }

    @action
    logout() {
        return Axios.post('/public/rpc', {
            method: "auth.logout"
        })
            .then(() => this.setUser(null))
            .catch((e) => {
                console.error("LOGOUT ERROR: ", e)
            });
    }

    @action
    fetchSettings() {
        return Axios.post('/public/rpc', {
            method: "UserSettings.get"
        })
            .then((response) => {
                let result = response.data.result;

                if (result) {

                    this.user.settings = {
                        ...this.user.settings,
                        ...result
                    };

                    if (this.settingsCallback) {
                        this.settingsCallback(this.user.settings);
                    }

                    this.refresh = result.refresh;

                    if (this.hash === undefined) {
                        this.hash = result.hash
                    }

                    if (this.refresh === true) {
                        if (this.hash === result.hash) {
                            if (this.timeout) {
                                clearTimeout(this.timeout)
                            }
                            this.timeout = setTimeout(() => {
                                this.fetchSettings()
                            }, this.refreshInterval)
                        } else {
                            if (this.timeout) {
                                clearTimeout(this.timeout)
                            }
                            document.location.reload()
                        }
                    }

                }
            })
            .catch((e) => {
                console.error("SETTINGS FETCH ERROR: ", e)
            });
    }


    @action
    setUser(userData) {
        this.user = userData
    }

    @computed get systemLocale() {
        return window.localStorage.getItem("system_locale") || (this.user ? this.user.settings.language : null)
    }

    @computed
    get permissions() {
        return (this.user || {}).permissions || []
    }

    hasPerm(permission) {
        return this.permissions.includes(permission)
    }

    /*
    if user has any of permissions in array => true
     */
    hasAnyPerm(permission) {
        if (typeof permission === "string") {
            return userStore.hasPerm(permission)
        }

        if (Array.isArray(permission)) {
            return permission.some(p => userStore.hasPerm(p))
        }

        return true
    }
}


const userStore = new UserStore();
export default userStore;