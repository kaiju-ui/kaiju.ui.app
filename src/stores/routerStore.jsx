import {RouterStore as RS} from 'mobx-router5';
import BaseStorage from "@kaiju.ui/components/src/stores/storage";

// I instantiate it here because I could add something to the class before invoking new
class RouterStore extends RS {

    /*
    Добавляет параметры в урл и в session storage

    values = {
        some_param: 1,
        break: "dance",
    },

     */
    setParams(values) {
        Object.entries(values).forEach(([name, value]) => {
            let storageKey = this.route.name + "." + name
            BaseStorage.setItem(storageKey, value);
            this.setQueryParam(name, value)
        });

    }

    setQueryParam = (name, value) => {
        let param = {}
        param[name] = value
        this.route.params[name] = value;
        let newUrl = this.router.buildPath(this.route.name, this.route.params);
        history.replaceState({ ...this.route,  params: {...this.route.params, ...param}}, this.route.name, newUrl);
    };

    /*
    Берет параметр из урла
     */
    getParam = (name, setToUrl = true, setToStorage = true) => {
        const route = this.route;
        const storageKey = route.name + "." + name;

        let valueInUrl = route.params[name]
        let valueInStorage = BaseStorage.getItem(storageKey, "")

        if (typeof valueInUrl === "boolean" || valueInUrl) {
            if (["true", "false"].includes(valueInUrl)) {
                valueInUrl = JSON.parse(valueInUrl);
            }
            setToStorage && BaseStorage.setItem(storageKey, valueInUrl);
            return valueInUrl
        } else if (typeof valueInStorage === "boolean" || valueInStorage) {
            setToUrl && this.setQueryParam(name, valueInStorage)
            return valueInStorage
        } else {
            return undefined
        }
    }


}

const routerStore = new RouterStore();
export default routerStore;

