import systemRoutesMap from "System/systemRoutes";
import systemRoutesNames from "System/systemRoutesNames";


export {
    systemRoutesNames,
    systemRoutesMap
}