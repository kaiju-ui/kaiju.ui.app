import React from "react";
import {inject} from "mobx-react";
import {routeNode, RouteView} from "react-mobx-router5";
import systemRoutesNames from "System/systemRoutesNames";

const routeNodeName = systemRoutesNames.usersRoot;

@inject("routerStore")
class Users extends React.Component {

    render() {
        const {route, routerStore} = this.props;

        return (
            <React.Fragment>
                <RouteView route={route} routes={routerStore.routesMap} routeNodeName={routeNodeName}/>
            </React.Fragment>
        )
    }
}


export default routeNode(routeNodeName)(Users);
