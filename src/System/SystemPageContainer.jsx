import React from "react";
import {Sidebar} from "Nav/Sidebar";

import {inject} from "mobx-react";
import {routeNode, RouteView} from "react-mobx-router5";
import loadSidebarConf from "./sidebarConf";
import systemRoutesNames from "System/systemRoutesNames";
import PageContentComponent from "Layout/PageContentComponent";

const routeNodeName = systemRoutesNames.root;

@inject("routerStore")
class SystemPageContainer extends React.Component {
    sidebarConf = loadSidebarConf();

    render() {
        const {route, routerStore} = this.props;

        return (
            <React.Fragment>
                <Sidebar conf={this.sidebarConf}/>
                <PageContentComponent>
                    <RouteView route={route} routes={routerStore.routesMap} routeNodeName={routeNodeName}/>
                </PageContentComponent>
            </React.Fragment>
        )
    }
}


export default routeNode(routeNodeName)(SystemPageContainer);