import SystemPageContainer from "./SystemPageContainer";
import UsersPage from "./UsersPage";
import Users from "./nodecomponents/Users";
import Groups from "./nodecomponents/Groups";
import UserEditPage from "./UserEditPage";
import GroupsPage from "./GroupsPage";
import GroupsEditPage from "./GroupEditPage/GroupsEditPage";


const systemRoutesMap = {
    name: "system",
    path: "/system",
    component: SystemPageContainer,
    children: [
        {
            name: "groups",
            path: "/groups",
            component: Groups,
            children: [
                {
                    name: "all",
                    path: "/",
                    component: GroupsPage
                },
                {
                    name: "edit",
                    path: "/:id",
                    component: GroupsEditPage
                },
            ]
        },
        {
            name: "users",
            path: "/users",
            component: Users,
            children: [
                {
                    name: "all",
                    path: "/",
                    component: UsersPage
                },
                {
                    name: "edit",
                    path: "/:idOrUsername",
                    component: UserEditPage
                },
            ]
        },
    ]
};


export default systemRoutesMap;
