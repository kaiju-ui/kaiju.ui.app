import React from "react";
import SearchBox from "@kaiju.ui/components/src/SearchComponent";
import CheckboxComponent from "@kaiju.ui/components/src/Checkbox";
import {observer} from "mobx-react";


@observer
class PermissionGroupInfo extends React.Component {

    renderItem(permission, index) {
        return (
            <li style={{listStyle: "none"}} key={utils.uuid4()}>
                <CheckboxComponent
                    onChange={(value) => (
                        this.props.store.currentGroup.updatePermission(index, value)
                    )}
                    className={" mt-3 ml-3 mb-0"}
                    defaultChecked={permission.value}
                    label={permission.label}/>
            </li>
        )
    }

    render() {

        return (
            <React.Fragment>
                <div className={this.props.className || ""}>
                    {
                        (!this.props.store.actions.isFetching && this.props.store.currentGroup) &&
                        <React.Fragment>
                            <div className="form-group pb-4 pt-0 border-bottom">
                                <span className="m--icon-font-size-lg1 m--font-bolder">
                                    {this.props.store.currentGroup &&
                                    this.props.store.currentGroup.id}
                                </span>
                            </div>

                            <ul style={{paddingLeft: 0}}>
                                {
                                    (this.props.store.currentGroup.permissions || []).map(
                                        (permission, index) => this.renderItem(permission, index)
                                    )
                                }
                            </ul>
                        </React.Fragment>
                    }
                </div>
            </React.Fragment>
        )
    }
}

@observer
class PermissionGroups extends React.Component {

    renderGroup(groupStore) {
        return (
            <div key={utils.uuid4()}
                 style={{wordBreak: "break-word"}}
                 className="border-bottom hover-grey p-3 ">

                <CheckboxComponent
                    onChange={(checked) => {
                        groupStore.updateAllPermissions(checked)
                    }}
                    indeterminate={groupStore.isAnyChecked}
                    defaultChecked={groupStore.isAllChecked || groupStore.isAnyChecked}
                />

                <span
                    className="pointer"
                    onClick={() => groupStore.showGroup()}>
                    {groupStore.id}
                </span>
            </div>
        )
    }


    render() {
        return <div className={this.props.className || ""}>

            <SearchBox
                className="p-4 border-bottom"
                callback={(query) => this.props.store.actions.setQuery(query)}/>

            <div style={{height: "65vh", overflowY: "scroll"}} className="hide-scroll">
                {
                    (this.props.store.actions.filteredPermissions || []).map((group) =>
                        this.renderGroup(group)
                    )
                }
            </div>

        </div>
    }
}


class PermissionsComponent extends React.Component {
    render() {
        return (
            <div className="row pt-3">
                <PermissionGroups store={this.props.store}
                                  className="col-3 border"/>
                <PermissionGroupInfo store={this.props.store} className="col-9 pl-5"/>
            </div>
        )
    }
}


export default PermissionsComponent;
