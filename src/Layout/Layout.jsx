import React from "react";
import {MainMenu} from "Nav";

import {inject, observer} from "mobx-react";
import ContentBody from "Pages/ContentBody/ContentBody";
/** Разметка страницы . */

@inject("routerStore")
@observer
export default class MainMenuLayout extends React.Component {

    render() {
        return (
            <React.Fragment>
                <MainMenu
                    navigateToProfile={() => this.props.navigateToProfile()}
                    conf={this.props.loadMainMenuConf()}/>
                <ContentBody routerStore={this.props.routerStore}/>
            </React.Fragment>
        )
    }
}