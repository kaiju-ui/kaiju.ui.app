import React from "react";
import "./Layout.scss"

export default class PageContentComponent extends React.Component {
    render() {
        return (
            <div className="page-content">
                <div className="page-content__inner">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

 class NoSidebarPageContent extends React.Component {
    render() {
        return (
            <div className="no-side-page-content">
                <div className="page-content__inner">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export {NoSidebarPageContent}