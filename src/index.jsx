import ErrorPage from "Pages/ErrorPage";
import * as Loginkins from "Login"
import * as Routinkins from "router"
import * as stores from "stores"
import * as Navs from "Nav"
import * as Layouts from "Layout"
import * as Systems from "System"
import * as Tasks from "Tasks"

export {
    Navs,
    Layouts,
    ErrorPage,
    Loginkins,
    Routinkins,
    stores,
    Systems,
    Tasks
}