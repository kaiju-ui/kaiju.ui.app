import React from "react";
import DynamicHeader from "@kaiju.ui/components/src/Header/DynamicHeader";
import {Tab, TabList, TabPanel, Tabs} from "@kaiju.ui/components/src/Tabs";
import BaseStorage from "@kaiju.ui/components/src/stores/storage";
import Form from "@kaiju.ui/forms/src/Form";
import {inject, observer, Provider} from "mobx-react";
import Loader from "@kaiju.ui/components/src/Loader";
import asyncErrorDecorator from "router/asyncErrorComponent";
import EditFormStore from "@kaiju.ui/forms/src/Form/EditFormStore";
import ReactJson from "react-json-view";
import Axios from "axios";
import {action, observable} from "mobx";
import TaskStore from "Tasks/TaskStore";

import PropTypes from "prop-types";
import {ButtonDefault} from "@kaiju.ui/components/src/Buttons";
import {notifySuccess} from "@kaiju.ui/components/src/notifications";
import WindowActionComponent from "@kaiju.ui/components/src/Window/ActionComponent";
import MetaTitle from "@kaiju.ui/components/src/Header/MetaTitle";
import {TableStore} from "@kaiju.ui/components/src/Tables/stores";
import TableComponent from "@kaiju.ui/components/src/Tables/TableComponent";
import tableCellConstructors, {tableFieldConstructors} from "@kaiju.ui/components/src/Tables/constructors";

class WindowConfirmComponent extends WindowActionComponent {
    confirmText = "Эта задача точно не выполняется?"
    iconClass = "icon-info"
    cancelLabel = "нет, проверю"
    confirmLabel = "да, точно"
}

@asyncErrorDecorator()
@inject("routerStore", "asyncErrorStore")
@observer
export default class TasksEditPage extends React.Component {
    @observable task;

    constructor(props) {
        super(props);
        this.id = this.props.routerStore.route.params.id;
        this.serviceName = this.props.serviceName || "tasks"
        this.notificationServiceName = this.props.notificationServiceName || "notifications"
        this.taskStore = new TaskStore(this.serviceName)
        this.breadcrumbs = this.props.breadcrumbs || [];

        this.formHandleStore = new EditFormStore({
                labelKey: "task_name",
                formStore: {
                    hasHeader: false,
                    requests: {
                        load: {
                            method: `${this.serviceName}.get`,
                            params: {
                                id: this.id,
                                grouping: true
                            },
                            errorCallback: (responseData) => {
                                let errorCode = responseData.error ? responseData.error.code : '';
                                this.props.asyncErrorStore.setError(errorCode)
                            },
                        },
                        save: {
                            method: `${this.serviceName}.update`,
                            params: {
                                id: this.id
                            }
                        }
                    },
                }
            }
        );

        if (this.notificationServiceName) {
            this.notificationTableStore = new TableStore({
                request: {
                    method: `${this.notificationServiceName}.grid`,
                    params: {
                        task_id: this.id
                    },
                    errorCallback: (responseData) => utils.errorCallback({responseData, ...this.props}),
                },
                fieldConstructors: {
                    exit_code: ({value}) => {
                        if (value === 0) {
                            return (
                                <span style={{color: "green", fontWeight: "bold"}}>
                                {utils.getTranslation("TasksExitCode.0")}
                            </span>
                            )
                        } else if (value === 130) {
                            return (
                                <span style={{color: "red", fontWeight: "bold"}}>
                                {utils.getTranslation("TasksExitCode.130")}
                            </span>
                            )

                        } else if (value === 1) {
                            return (
                                <span style={{color: "red", fontWeight: "bold"}}>
                                {utils.getTranslation("TasksExitCode.1")}
                            </span>
                            )
                        }
                    },
                    status: (row) => {
                        if (row.value) {
                            return <div
                                className={"m-w-85"}>{utils.getTranslation(`TaskStatus.${row.value.replace("tasks.status.", "")}`)}</div>
                        }
                        return <React.Fragment/>
                    },
                    result: tableFieldConstructors["task_result"]
                }
            });
        }
    }

    componentDidMount() {
        this.loadTask()
    }

    @action
    loadTask() {
        Axios.post('/public/rpc',
            {
                method: `${this.serviceName}.get`,
                params: {
                    id: this.id
                }
            }
        ).then(response => {
            if (response.data.result) {
                this.task = response.data.result;
            } else {
                utils.handleNotifyExceptions(response)
            }
        }).catch(response => {
            utils.handleNotifyExceptions(response)
        })
    }

    @action
    forceIdle() {
        Axios.post('/public/rpc',
            {
                method: `${this.serviceName}.force_idle`,
                params: {
                    id: this.id
                }
            }
        ).then(response => {
            if (response.data.result) {
                this.loadTask()
                notifySuccess()
            } else {
                utils.handleNotifyExceptions(response)
            }
        }).catch(response => {
            utils.handleNotifyExceptions(response)
        })
    }


    DynamicHeader = observer(() => {
        let {status = "", active = false} = this.task || {};

        const dropDownConfs = [
            {
                label: utils.getTranslation("Task.restart"),
                callback: () => {
                    confirm(`${utils.getTranslation("Task.restart")}?`) &&
                    this.taskStore.restartTask(this.id).then(
                        () => {
                            this.formHandleStore.reloadForms()
                            this.loadTask()
                        }
                    )
                },
            }
        ]

        if (this.taskStore.canAbort(status, active)) {
            dropDownConfs.push({
                label: utils.getTranslation("Task.abort"),
                callback: () => {
                    confirm(`${utils.getTranslation("Task.abort")}?`) &&
                    this.taskStore.abortTask(this.id).then(
                        () => {
                            this.formHandleStore.reloadForms()
                            this.loadTask()
                        }
                    )
                },
            })
        }

        if (this.props.disableDelete !== true) {
            dropDownConfs.push({
                label: utils.getTranslation("Task.delete"),
                callback: () => {
                    confirm(`${utils.getTranslation("Message.delete")}?`) &&
                    this.taskStore.deleteTask(this.id).then(
                        () => this.props.deleteCallback());
                },
            },)
        }

        this.doubleButtonConf = {
            mainButtonConf: {
                label: utils.getTranslation("Button.save"),
                icon: "icon-plus",
                onClick: () => {
                    this.formHandleStore.saveCallback();
                }
            },
            dropDownConfs: dropDownConfs
        };

        let breadcrumbs = [
            ...this.breadcrumbs,
            {
                label: this.formHandleStore.label
            }
        ]


        return (
            <>
                <MetaTitle breadcrumbs={breadcrumbs}/>
                <DynamicHeader
                    breadcrumbs={this.breadcrumbs}
                    doubleButtonConf={this.doubleButtonConf}
                    isChanged={this.formHandleStore.isChanged}
                    isFetching={this.formHandleStore.isFetching}
                    label={this.formHandleStore.label}
                />
            </>
        )
    })

    render() {
        let {formStore} = this.formHandleStore;

        return (
            <React.Fragment>
                <Loader show={!this.formHandleStore.dataInitialized || this.formHandleStore.isFetching}/>
                <this.DynamicHeader/>

                <Tabs className="page-content-body pl-5 pr-5"
                      defaultIndex={BaseStorage.getItem(this.props.route.name, 0)}
                      onSelect={(index) => BaseStorage.setItem(this.props.route.name, index)}>

                    <TabList>
                        <Tab>{utils.getTranslation("Tab.properties")}</Tab>
                        <Tab>{utils.getTranslation("Task.commands")}</Tab>
                        <Tab>{utils.getTranslation("Task.result")}</Tab>
                        {this.notificationServiceName && <Tab>{utils.getTranslation("Task.notifications")}</Tab>}
                    </TabList>

                    <TabPanel>
                        {
                            this.formHandleStore.dataInitialized &&
                            <React.Fragment>
                                <div className={this.formHandleStore.isFetching ? "disabled-form" : ""}>
                                    <Form key={formStore.key}
                                          store={formStore}
                                          groupFiltering/>
                                </div>
                            </React.Fragment>
                        }
                        {
                            this.task?.status === "EXECUTED" && (
                                <WindowConfirmComponent
                                    idLabel={this.task.id}
                                    actionLabel={this.task.name}
                                    actionCallback={() => this.forceIdle()}>
                                    <ButtonDefault
                                        className={"mt-2 mb-2"}
                                        label={utils.getTranslation("Task.force_idle")}
                                    />
                                </WindowConfirmComponent>
                            )
                        }
                    </TabPanel>
                    <TabPanel>
                        {
                            this.task && <ReactJson
                                {...(this.props.jsonProps || {})}
                                src={this.task.commands || {}}
                                name={null}/>
                        }
                    </TabPanel>
                    <TabPanel>
                        {
                            this.task &&
                            <ReactJson
                                {...(this.props.jsonProps || {})}
                                src={this.task.result || {}}
                                name={null}/>
                        }
                    </TabPanel>

                    {
                        this.notificationServiceName && (
                            <TabPanel>
                                <Provider tableStore={this.notificationTableStore}>
                                    <TableComponent className="pb-5" key={utils.uuid4()}/>
                                </Provider>
                            </TabPanel>
                        )
                    }

                </Tabs>

            </React.Fragment>
        )
    }
}

TasksEditPage.propTypes = {
    breadcrumbs: PropTypes.array.isRequired,
    deleteCallback: PropTypes.func.isRequired,
    jsonProps: PropTypes.object,
    route: PropTypes.object,
    routerStore: PropTypes.object,
    asyncErrorStore: PropTypes.object,
};