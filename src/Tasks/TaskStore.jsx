import Axios from "axios";
import {notifySuccess} from "@kaiju.ui/components/src/notifications";

class TaskStore {
    isFetching = false;

    constructor(serviceName) {
        this.serviceName = serviceName;
    }


    deleteTask(id) {
        if (this.isFetching) {
            return
        }

        this.isFetching = true;

        return Axios.post('/public/rpc', {
            method: `${this.serviceName}.delete`,
            params: {
                id: id,
            },
        }).then((response) => {
            if (!response?.data?.error) {
                notifySuccess();
                return
            }

            notifyError(response?.error?.message, 3000);
            return Promise.reject()
        }).catch((response) => {
            utils.handleNotifyExceptions(response);
        }).finally((response) => {
            this.isFetching = false;
        })
    }

    abortTask(id) {
        if (this.isFetching) {
            return
        }

        this.isFetching = true;

        return Axios.post('/public/rpc', {
            method: `${this.serviceName}.abort`,
            params: {
                id: id,
            },
        }).then((response) => {
            if (!response.data?.error) {
                notifySuccess();
                return
            }

            notifyError(response.data?.error?.message, 3000);
            return Promise.reject()
        }).catch((response) => {
            utils.handleNotifyExceptions(response);
        }).finally((response) => {
            this.isFetching = false;
        })
    }

    restartTask(id) {
        if (this.isFetching) {
            return
        }
        this.isFetching = true;

        return Axios.post('/public/rpc', {
            "method": `${this.serviceName}.abort_and_restart`,
            "params": {
                "id": id
            }
        }).then((response) => {
            if (!response?.data?.error) {
                notifySuccess();
                return
            }

            utils.handleNotifyExceptions(response.data.error);
        }).catch((response) => {
            utils.handleNotifyExceptions(response);
        }).finally((response) => {
            this.isFetching = false;
        })
    }

    canAbort(status, active) {
        return status.toLowerCase() === 'idle' && active === true
    }

    canRestart(status, active) {
        return (['finished', 'aborted'].includes(status.toLowerCase())) && active === true
    }

}

export default TaskStore;