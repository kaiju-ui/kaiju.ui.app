import React from "react";
import {Header} from "@kaiju.ui/components/src/Header";
import TableComponent from "@kaiju.ui/components/src/Tables/TableComponent";
import {inject, Provider} from "mobx-react";
import {TableStore} from "@kaiju.ui/components/src/Tables/stores";
import asyncErrorDecorator from "router/asyncErrorComponent";
import ActionsComponent, {ActionIcon, DeleteAction} from "@kaiju.ui/components/src/Tables/actions";
import TaskStore from "./TaskStore";
import PropTypes from "prop-types";
import {SimpleSelectComponent} from "@kaiju.ui/components/src/Select/SimpleSelect";
import BaseStorage from "@kaiju.ui/components/src/stores/storage";
import cronstrue from "cronstrue"
import {expandResultCell} from "Tasks/tableCells";
import {progressCell} from "@kaiju.ui/components/src/Tables/extraCells";
import 'cronstrue/locales/ru.min.js';
import "./TasksPage.scss"

@inject("rowStore", "tableStore", "taskStore")
class TableActionsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.id = this.props.rowStore.id;

        this.label = this.props.rowStore.data?.task_name?.value ||
            this.props.rowStore.data?.label?.value || this.id;
    }

    delete() {
        this.props.taskStore.deleteTask(this.id).then(
            () => this.props.tableStore.actions.fetchByPage()
        );
    }

    restart() {
        confirm(`${utils.getTranslation("Task.restart")} "${this.label}"?`) &&
        this.props.taskStore.restartTask(this.id).then(
            () => this.props.tableStore.actions.fetchByPage()
        );
    }

    render() {

        return (
            <ActionsComponent className={"m-w-85"}>
                <ActionIcon
                    title={utils.getTranslation("Task.restart")}
                    iconClass="icon-plus"
                    callback={() => this.restart()}/>
                {
                    this.props.disableDelete !== true && (
                        <DeleteAction
                            idLabel={this.label}
                            actionLabel={utils.getTranslation("Task.delete")}
                            deleteCallback={() => this.delete()}
                        />
                    )
                }
            </ActionsComponent>
        );
    }
}

@asyncErrorDecorator()
@inject("routerStore", "userStore")
export default class TasksPage extends React.Component {

    constructor(props) {
        super(props);
        this.serviceName = this.props.serviceName || "tasks";
        this.taskStore = new TaskStore(this.serviceName);

        this.fieldConstructors = {
            progress: progressCell,
            status: (row) => {
                if (row.value) {
                    return <div className={"m-w-85"}>{utils.getTranslation(`TaskStatus.${row.value}`)}</div>
                }
                return <React.Fragment/>
            },
            next_run: (row) => {
                if (!row.value) {
                    return <React.Fragment>{utils.getTranslation("Task.not_planned")}</React.Fragment>
                }
                return <React.Fragment>{row.value}</React.Fragment>
            },
            cron: ({value}) => {
                if (!value) {
                    return <></>
                }

                return (
                    <span className="cron-cell">
                            {cronstrue.toString(value, {locale: this.props.userStore.systemLocale.slice(0, 2)})}
                        </span>
                )
            },
            exit_code: ({value}) => {
                if (value === 0) {
                    return (
                        <span style={{color: "green", fontWeight: "bold"}}>
                                {utils.getTranslation("TasksExitCode.0")}
                            </span>
                    )
                } else if (value === 130) {
                    return (
                        <span style={{color: "red", fontWeight: "bold"}}>
                                {utils.getTranslation("TasksExitCode.130")}
                            </span>
                    )

                } else if (value === 1) {
                    return (
                        <span style={{color: "red", fontWeight: "bold"}}>
                                {utils.getTranslation("TasksExitCode.1")}
                            </span>
                    )
                }
            },
            ...(this.props.fieldConstructors || {}),
        }

        if (this.props.expandResult) {
            this.fieldConstructors["result"] = expandResultCell
        }

        this.tableStore = new TableStore({
            page: utils.getPage(),
            autoRefresh: this.props.autoRefresh || 15,
            paginationCallback: utils.paginationCallback,
            request: {
                method: `${this.serviceName}.grid`,
                params: {
                    locale: this.props.userStore.systemLocale,
                    query: utils.getQuery(),
                    with_cron: BaseStorage.getItem("tasks.cron", {})?.value,
                    ...(this.props.params || {})
                }
            },
            fieldConstructors: this.fieldConstructors,
            ActionsComponent: (props) => <TableActionsComponent {...props} disableDelete={this.props.disableDelete}/>,
            rowCallback: (rowStore) => this.props.rowCallback(rowStore),
        });

        this.cronFilterConf = {
            label: utils.getTranslation("Task.period_task"),
            defaultValue: BaseStorage.getItem("tasks.cron") || {
                "label": utils.getTranslation("all"),
                "value": null
            },
            options: [
                {
                    "label": utils.getTranslation("all"),
                    "value": null
                },
                {
                    "label": utils.getTranslation("Condition.yes"),
                    "value": true
                },
                {
                    "label": utils.getTranslation("Condition.no"),
                    "value": false
                }
            ],
            onChange: (values) => {
                BaseStorage.setItem("tasks.cron", values);
                this.tableStore.conf.request.params["with_cron"] = values.value;
                this.tableStore.actions.fetchByPage(1);
            }
        };

    }

    searchCallback = (value) => {
        utils.queryCallback(value);
        this.tableStore.actions.fetchByQuery(value);
    }

    render() {
        return (
            <React.Fragment>
                {
                    this.props.disableHeader !== true && <Header
                        breadcrumbs={this.props.breadcrumbs}
                        defaultValue={utils.getQuery()}
                        searchCallback={this.searchCallback}
                    />
                }
                <div id={this.props.tableId || "table"}>
                    <div className="table-dropdowns">
                        <div className="right-dropdown pr-5">
                            <SimpleSelectComponent conf={this.cronFilterConf}/>
                        </div>
                    </div>
                    <Provider tableStore={this.tableStore} taskStore={this.taskStore}>
                        <TableComponent
                            paginationClassName={this.props.paginationClassName || "pl-5 pr-5"}
                            className={this.props.tableClassName || "pb-5 pl-5 pr-5"}
                            key={utils.uuid4()}/>
                    </Provider>
                </div>
            </React.Fragment>
        )
    }
}


TasksPage.propTypes = {
    breadcrumbs: PropTypes.array,
    rowCallback: PropTypes.func.isRequired,
    jsonProps: PropTypes.object,
    autoRefresh: PropTypes.number,
    route: PropTypes.object,
    routerStore: PropTypes.object,
    userStore: PropTypes.object,
    asyncErrorStore: PropTypes.object,
};