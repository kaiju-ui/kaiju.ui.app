import TasksEditPage from "./TasksEditPage";
import TasksPage from "./TasksPage";

export {
    TasksEditPage,
    TasksPage,
}