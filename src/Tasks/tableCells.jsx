import React from "react";
import tableCellConstructors from "@kaiju.ui/components/src/Tables/constructors";


const expandResultCell = (cell) => {
    const handleValue = (value) => {
        let result

        try {
            result = value[0]['RESULT']?.result
        } catch (e) {
            result = value[0]['CMD']
            result = "..."
        }

        if (typeof (result) === "object" && !result.hasOwnProperty("error")) {
            return (
                <ul className="text-left">
                    {
                        Object.keys(result).map((key) => {

                            return (
                                <li key={key}>
                                    {utils.getTranslation(key)}: <b>{result[key]}</b>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        } else {
            result = "{...}"
        }

        return (
            <span>{result}</span>
        )
    }

    return tableCellConstructors["json_object"](cell, handleValue)
}

export {expandResultCell};
