import React from "react";
import ReactTooltip from "react-tooltip";
import {inject} from "mobx-react";
import {toJS} from "mobx";
import "Nav/MainMenu/UserMenuLink.scss"

@inject("userStore")
@inject("routerStore")
class UserMenuLink extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            img: "/static/img/default_user.png"
        }
    }


    renderTooltip() {
        let fullName = toJS(this.props.userStore.user)["full_name"];

        return (
            <ul className="m-nav m-nav--skin-light pl-3">
                <li className="m-nav__item">
                        <span className="m-nav__link-title">
                            <span className="m-nav__link-wrap">
                                <span className="m-nav__link-text">{fullName}</span>
                            </span>
                        </span>
                </li>
                <li className="m-nav__separator m-nav__separator--fit"/>
                <li className="m-nav__item pb-3 pointer" onClick={() => this.props.navigateToProfile()}>
                        <span className="m-nav__link-title">
                            <span className="m-nav__link-wrap">
                                <span className="m-nav__link-text">{utils.getTranslation("User.profile")}</span>
                            </span>
                        </span>
                </li>
                <li className="m-nav__item pointer" onClick={() => {
                    this.props.userStore.logout().then(() => this.props.routerStore.router.navigate("login"))
                }}>
                        <span className="m-nav__link-title">
                            <span className="m-nav__link-wrap">
                                <span className="m-nav__link-text">{utils.getTranslation("User.logout")}</span>
                            </span>
                        </span>
                </li>
            </ul>
        )
    }

    render() {
        const id = utils.uuid4();

        return (
            <li className="main-menu__item">
                <a className="main-menu__link" data-for={id} data-tip>
                    <i className="main-menu__icon m-menu__link-icon icon-user fs-2_5"/>
                </a>

                <ReactTooltip
                    id={id}
                    className="main-menu__tooltip main-menu__tooltip_user"
                    aria-haspopup={true}
                    scrollHide={true}
                    delayHide={100}
                    place="right"
                    effect="solid">
                    {this.renderTooltip()}
                </ReactTooltip>

            </li>
        );
    }


}

export default UserMenuLink;